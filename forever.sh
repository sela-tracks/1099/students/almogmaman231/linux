#!/bin/bash

while true; do
    echo "This is an infinite loop. Hit Ctrl+C to stop."
    sleep 1  # Pause for 1 second before the next iteration
done
